using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace ExchangeService.Api.Tests.Applications
{
    public class FakeHttpMessageHandler : HttpMessageHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken) =>
            await Task.Run(() => new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(@"
{
    ""success"": true,
    ""timestamp"": 1519296206,
    ""base"": ""USD"",
    ""date"": ""2021-06-18"",
    ""rates"": {
        ""GBP"": 0.72007,
        ""JPY"": 107.346001,
        ""EUR"": 0.813399
        }
}  
")
            }, cancellationToken).ConfigureAwait(false);
    }
}
