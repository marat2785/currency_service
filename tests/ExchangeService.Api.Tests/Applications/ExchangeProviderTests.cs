using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using ExchangeService.Api.Applications;
using ExchangeService.Api.Dto;
using ExchangeService.Api.Infrastructure.Configurations;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Xunit;

namespace ExchangeService.Api.Tests.Applications
{
    public class ExchangeProviderTests
    {
        [Fact]
        public async Task Should_Return_Correct_Exchanges()
        {
            var now = DateTime.Now.Date;

            var httpClientFactory = Substitute.For<IHttpClientFactory>();
            var cbrSettings = new ExchangeSourceSettings {BaseUri = new Uri("http://any.ru"), AccessKey = "", Symbols = new List<string>(), BaseCurrency = ""};

            httpClientFactory.CreateClient().ReturnsForAnyArgs(_ => new HttpClient(new FakeHttpMessageHandler()));

            var logger = Substitute.For<ILogger<ExchangeProvider>>();

            var provider = new ExchangeProvider(httpClientFactory, cbrSettings, logger);
            var exchanges = await provider.GetCurrencyAsync(now, CancellationToken.None);

            var expected = new List<Exchange>
            {
                new() {Date = now, Currency = "GBP", Rate = 0.72007M},
                new() {Date = now, Currency = "JPY", Rate = 107.346001M},
                new() {Date = now, Currency = "EUR", Rate = 0.813399M}
            };

            exchanges.Should().HaveCount(expected.Count);
            exchanges.Should().BeEquivalentTo(expected);
        }
    }
}

        