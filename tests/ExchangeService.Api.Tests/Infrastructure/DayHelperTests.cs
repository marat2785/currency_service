using System;
using System.Collections.Generic;
using ExchangeService.Api.Applications;
using ExchangeService.Api.Infrastructure;
using ExchangeService.Api.Infrastructure.Controllers.Models;
using FluentAssertions;
using Xunit;
using Xunit.Abstractions;

namespace ExchangeService.Api.Tests.Infrastructure
{
    public class DayHelperTests
    {
        private readonly ITestOutputHelper _log;

        public DayHelperTests(ITestOutputHelper log)
        {
            _log = log ?? throw new ArgumentNullException(nameof(log));
        }

        [Theory]
        [MemberData(nameof(Points))]
        public void TestDays(Point point, Day expected) => point.ToDay(4).Should().Be(expected);

        public static  IEnumerable<object[]> Points()
        {
            yield return new object[] {new Point {X = 2, Y = 2}, Day.Today};
            yield return new object[] {new Point {X = 2, Y = -2}, Day.Tomorrow};
            yield return new object[] {new Point {X = -2, Y = -2}, Day.DayBeforeYesterday};
            yield return new object[] {new Point {X = -2, Y = 2}, Day.Yesterday};
        }
    }
}
