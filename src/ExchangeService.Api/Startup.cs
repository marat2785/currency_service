using System;
using ExchangeService.Api.Adapters;
using ExchangeService.Api.Applications;
using ExchangeService.Api.Infrastructure;
using ExchangeService.Api.Infrastructure.Cache;
using ExchangeService.Api.Infrastructure.Configurations;
using ExchangeService.Api.Infrastructure.Controllers.Validators;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace ExchangeService.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.RegisterSettings(Configuration);

            services.AddHttpClient();
            services.AddMemoryCache();
            services.AddTransient<ICurrencyService, Applications.CurrencyService>();
            services.AddTransient<IExchangeProvider, ExchangeProvider>();
            services.AddSingleton<ICacheProvider, MemoryCacheProvider>();

            services.AddControllers(options=>
            {
                options.Filters.Add<ExceptionFilter>();
                options.Filters.Add<LoggerFilter>();

            }).AddFluentValidation(configuration => configuration.RegisterValidatorsFromAssemblyContaining<PointValidator>());

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "ExchangeService.Api", Version = "v1",
                    Contact = new OpenApiContact
                    {
                        Email = "marat2785@gmail.com",
                        Name = "����� ��������",
                        Url = new Uri("https://hh.ru/resume/e27d6d90ff00e7c8ea0039ed1f394234367251")
                    }
                });
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(options =>
                {
                    options.SwaggerEndpoint("/swagger/v1/swagger.json", "Hoff.CurrencyService.Api v1");
                });
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
