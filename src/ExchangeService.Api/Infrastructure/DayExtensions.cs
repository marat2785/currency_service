﻿using System;
using ExchangeService.Api.Applications;

namespace ExchangeService.Api.Infrastructure
{
    public static class DayExtensions
    {
        public static DateTime GetDate(this Day day)
        {
            var now = DateTime.UtcNow.Date;
            return day switch
            {
                Day.Today => now,
                Day.Yesterday => now.AddDays(-1),
                Day.DayBeforeYesterday => now.AddDays(-2),
                Day.Tomorrow => now.AddDays(1),
                _ => throw new ArgumentOutOfRangeException(nameof(day), day, $"Enum have unsupported value. Actual: {day}")
            };
        }
    }
}
