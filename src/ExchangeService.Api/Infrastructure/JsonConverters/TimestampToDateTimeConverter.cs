﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace ExchangeService.Api.Infrastructure.JsonConverters
{
    /// <summary>
    /// конвертор не очень безопасен, писалось на скорую руку.
    /// </summary>
    public class TimestampToDateTimeConverter : JsonConverter<DateTime>
    {
        public override bool CanConvert(Type typeToConvert) => typeToConvert == typeof(DateTime);

        public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) => DateTime.UnixEpoch.AddSeconds(reader.GetInt64());

        public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options) => writer.WriteNumberValue(value.AddSeconds(-DateTime.UnixEpoch.Second).Second);
    }
}
