﻿using System;
using System.Globalization;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace ExchangeService.Api.Infrastructure.JsonConverters
{
    public class StringToDateTimeConverter : JsonConverter<DateTime>
    {
        public override bool CanConvert(Type typeToConvert) => typeToConvert == typeof(DateTime);

        public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) =>
            DateTime.TryParse(reader.GetString(), new DateTimeFormatInfo {FullDateTimePattern = "yyyy-MM-dd"},
                DateTimeStyles.AllowTrailingWhite, out var date)
                ? date
                : DateTime.MinValue;

        public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options) => writer.WriteStringValue(JsonEncodedText.Encode(value.ToString("yyyy-MM-dd"), JavaScriptEncoder.Default));
    }
}
