﻿using System;
using ExchangeService.Api.Applications;
using ExchangeService.Api.Infrastructure.Controllers.Models;

namespace ExchangeService.Api.Infrastructure
{
    public static class PointExtensions
    {
        public static Day ToDay(this Point point, int radius)
        {
            return point switch
            {
                { } p when (p.X <= radius && p.X >= 0) && (p.Y <= radius && p.Y >= 0) => Day.Today,
                { } p when (p.X <= radius && p.X >= 0) && (-p.Y <= radius && p.Y <= 0) => Day.Tomorrow,
                { } p when (p.X >= -radius && p.X <= 0) && (p.Y >= -radius && p.Y <= 0) => Day.DayBeforeYesterday,
                { } p when (p.X >= -radius && p.X <= 0) && (p.Y <= radius && p.Y >= 0) => Day.Yesterday,
                {X: 0, Y: 0} => Day.Today,
                _ => throw new ArgumentOutOfRangeException(nameof(point), point, $"The coordinates don't fit into the circle. Actual: {nameof(point.X)}:{point.X}; {nameof(point.Y)}:{point.Y}")
            };
        }
    }
}
