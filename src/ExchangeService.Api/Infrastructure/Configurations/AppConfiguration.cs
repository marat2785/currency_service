﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ExchangeService.Api.Infrastructure.Configurations
{
    public static class AppConfiguration
    {
        public static void RegisterSettings(this IServiceCollection services, IConfiguration configuration)
        {
            var cbrSettings = configuration.GetSection("ExchangeSource").Get<ExchangeSourceSettings>();
            var apiSettings = configuration.GetSection("Api").Get<ApiSettings>();
            
            services.AddSingleton(cbrSettings);
            services.AddSingleton(apiSettings);
        }
    }
}