﻿namespace ExchangeService.Api.Infrastructure.Configurations
{
    public class ApiSettings
    {
        public int Radius { get; set; }
    }
}