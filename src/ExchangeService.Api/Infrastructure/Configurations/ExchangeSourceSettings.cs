﻿using System;
using System.Collections.Generic;

namespace ExchangeService.Api.Infrastructure.Configurations
{
    public class ExchangeSourceSettings
    {
        public Uri BaseUri { get; set; }

        public string AccessKey { get; set; }

        public string BaseCurrency { get; set; }

        public List<string> Symbols { get; set; }
    }
}