﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace ExchangeService.Api.Infrastructure
{
    public class ExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context) => context.Result = new BadRequestObjectResult($"Ой случилась ошибка!!! {context.Exception.Message}");
    }
}
