using System;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

namespace ExchangeService.Api.Infrastructure
{
    public class LoggerFilter : IExceptionFilter
    {
        private readonly ILogger<LoggerFilter> _logger;

        public LoggerFilter(ILogger<LoggerFilter> logger) => _logger = logger ?? throw new ArgumentNullException(nameof(logger));

        public void OnException(ExceptionContext context) => _logger.LogError(context.Exception, "log error handled");
    }
}
