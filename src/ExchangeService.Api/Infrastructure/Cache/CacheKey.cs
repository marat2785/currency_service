﻿namespace ExchangeService.Api.Infrastructure.Cache
{
    public class CacheKey
    {
        private CacheKey(string tag, string value)
        {
            Tag = tag;
            Value = value;
        }

        public string Tag { get; }

        public string Value { get; }

        public static CacheKey GenerateKey(string tag, string name, params object[] parameters) =>
            new CacheKey(tag, $"{tag}:{name}:{string.Join(":", parameters)}".TrimEnd(':'));
    }
}
