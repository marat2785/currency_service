﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ExchangeService.Api.Infrastructure.Cache
{
    public interface ICacheProvider
    {
        CacheKey GenerateKey(string tag, string name, params object[] parameters);

        Task<(bool, TValue)> TryGetAsync<TValue>(CacheKey key, CancellationToken cancellationToken);

        Task SetOrUpdateAsync<TValue>(CacheKey key, TValue value, TimeSpan? slidingExpiration, CancellationToken cancellationToken);

        Task InvalidateTagAsync(string tag, CancellationToken cancellationToken);
    }
}
