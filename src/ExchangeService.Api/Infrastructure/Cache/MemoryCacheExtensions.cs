﻿using System;
using Microsoft.Extensions.Caching.Memory;

namespace ExchangeService.Api.Infrastructure.Cache
{
    public static class MemoryCacheExtensions
    {
        public static void Set<TValue>(this IMemoryCache memoryCache,
            CacheKey key,
            TValue value,
            TimeSpan? slidingExpiration,
            Action<string, TValue, EvictionReason, CacheKey> evictionCallback)
        {
            memoryCache.Set(
                key: key.Value,
                value: value,
                options: new MemoryCacheEntryOptions
                {
                    SlidingExpiration = slidingExpiration,
                    PostEvictionCallbacks =
                    {
                        new PostEvictionCallbackRegistration
                        {
                            EvictionCallback = (callbackKey, callbackValue, reason, state) => evictionCallback((string)callbackKey, (TValue)callbackValue, reason, (CacheKey)state),
                            State = key
                        }
                    }
                });
        }
    }
}
