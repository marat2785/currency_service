﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;

namespace ExchangeService.Api.Infrastructure.Cache
{
    public class MemoryCacheProvider : ICacheProvider
    {
        private readonly IMemoryCache _memoryCache;
        private readonly ConcurrentDictionary<string, HashSet<string>> _tags = new();

        public MemoryCacheProvider(IMemoryCache memoryCache) =>
            _memoryCache = memoryCache ?? throw new ArgumentNullException(nameof(memoryCache));

        public CacheKey GenerateKey(string tag, string name, params object[] parameters) => CacheKey.GenerateKey(tag, name, parameters);

        public Task<(bool, TValue)> TryGetAsync<TValue>(CacheKey key, CancellationToken cancellationToken)
        {
            var result = _memoryCache.TryGetValue(key.Value, out TValue cachedValue)
                ? (true, cachedValue)
                : (false, default);

            return Task.FromResult<(bool, TValue)>(result);
        }

        private void OnRemove<TValue>(string rawKey, TValue value, EvictionReason reason, CacheKey key)
        {
            if (_tags.TryGetValue(key.Tag, out var keys))
            {
                //TODO: проверить что удаление работает нормально.
                lock (keys)
                {
                    keys.Remove(key.Value);
                }
            }
        }

        public Task SetOrUpdateAsync<TValue>(CacheKey key, TValue value, TimeSpan? slidingExpiration, CancellationToken cancellationToken)
        {
            _memoryCache.Set(key, value, slidingExpiration, OnRemove);

            // Для избежания потери ключа в HashSet
            if (!_tags.ContainsKey(key.Tag)) 
                _tags.TryAdd(key.Tag, new HashSet<string>());

            _tags.AddOrUpdate(key.Tag,
                addValueFactory: _ => new HashSet<string> { key.Value },
                updateValueFactory: (_, keys) =>
                {
                    lock (keys)
                    {
                        keys.Add(key.Value);
                        return keys;
                    }
                });

            return Task.CompletedTask;
        }

        public Task InvalidateTagAsync(string tag, CancellationToken cancellationToken)
        {
            if (_tags.TryRemove(tag, out var keys))
            {
                foreach (var key in keys)
                {
                    _memoryCache.Remove(key);
                }
            }

            return Task.CompletedTask;
        }
    }
}
