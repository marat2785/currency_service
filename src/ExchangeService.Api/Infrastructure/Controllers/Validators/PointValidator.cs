﻿using System;
using ExchangeService.Api.Infrastructure.Configurations;
using ExchangeService.Api.Infrastructure.Controllers.Models;
using FluentValidation;

namespace ExchangeService.Api.Infrastructure.Controllers.Validators
{
    public class PointValidator : AbstractValidator<Point>
    {
        public PointValidator(ApiSettings settings)
        {
            settings = settings ?? throw new ArgumentNullException(nameof(settings));

            RuleFor(x => x).Must(x => InFitRadius(x, settings.Radius))
                .WithMessage("The coordinates don't fit into the circle.");
        }

        private static bool InFitRadius(Point point, int radius) => Math.Pow(point.X, 2) + Math.Pow(point.Y, 2) <= Math.Pow(radius, 2);
    }
}
