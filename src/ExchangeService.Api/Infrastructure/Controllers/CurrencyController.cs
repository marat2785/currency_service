﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ExchangeService.Api.Adapters;
using ExchangeService.Api.Infrastructure.Configurations;
using ExchangeService.Api.Infrastructure.Controllers.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ExchangeService.Api.Infrastructure.Controllers
{
    [Produces("application/json")]
    [ApiController]
    [Route("api/[controller]")]
    public class CurrencyController : ControllerBase
    {
        private readonly ICurrencyService _currencyService;
        private readonly ApiSettings _apiSettings;
        private readonly ILogger<CurrencyController> _logger;

        public CurrencyController(ICurrencyService currencyService, ApiSettings apiSettings, ILogger<CurrencyController> logger)
        {
            _currencyService = currencyService ?? throw new ArgumentNullException(nameof(currencyService));
            _apiSettings = apiSettings ?? throw new ArgumentNullException(nameof(apiSettings));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpPost("GetExchange/{code}")]
        public async Task<IActionResult> GetExchange(string code, [FromBody] Point point, CancellationToken cancellationToken)
        {
            _logger.LogInformation($"The coordinates fit into a circle. Actual: {nameof(point.X)}:{point.X}; {nameof(point.Y)}:{point.Y}");

            var day = point.ToDay(_apiSettings.Radius);

            _logger.LogInformation($"Get exchange rate for currency: {code} on the day: {day}");

            var exchange = await _currencyService.GetExchangeByDays(code, day, cancellationToken);
            return Ok(exchange);
        }
    }
}
