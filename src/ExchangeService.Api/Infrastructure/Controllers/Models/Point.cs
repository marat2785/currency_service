﻿namespace ExchangeService.Api.Infrastructure.Controllers.Models
{
    public class Point
    {
        public int X { get; init; }

        public int Y { get; init; }
    }
}
