﻿using System;

namespace ExchangeService.Api.Dto
{
    /// <summary>
    /// Данные по курсу валюты
    /// </summary>
    public class Exchange
    {
        /// <summary>
        /// Текстовый код валюты по ISO
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Курс
        /// </summary>
        public decimal Rate { get; set; }
    }
}
