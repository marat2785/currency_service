﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using ExchangeService.Api.Adapters;
using ExchangeService.Api.Dto;
using ExchangeService.Api.Infrastructure.Configurations;
using ExchangeService.Api.Infrastructure.JsonConverters;
using Microsoft.Extensions.Logging;

namespace ExchangeService.Api.Applications
{
    public class ExchangeProvider : IExchangeProvider
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ExchangeSourceSettings _settings;
        private readonly ILogger<ExchangeProvider> _logger;

        private readonly JsonSerializerOptions _jsonSerializerOptions = new()
        {
            IgnoreNullValues = true,
            DictionaryKeyPolicy = JsonNamingPolicy.CamelCase,
            PropertyNameCaseInsensitive = true
        };

        public ExchangeProvider(IHttpClientFactory httpClientFactory,
            ExchangeSourceSettings settings,
            ILogger<ExchangeProvider> logger)
        {
            _httpClientFactory = httpClientFactory ?? throw new ArgumentNullException(nameof(httpClientFactory));
            _settings = settings ?? throw new ArgumentNullException(nameof(settings));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IList<Exchange>> GetCurrencyAsync(DateTime date, CancellationToken cancellationToken)
        {
            try
            {
                var query = new Uri(_settings.BaseUri, $"/api/{date:yyyy-MM-dd}?access_key={_settings.AccessKey}&base=EUR&symbols={string.Join(",",_settings.Symbols)}");
                using var httpClient = _httpClientFactory.CreateClient();
                
                var responseMessage = await httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Get, query), cancellationToken);

                if (responseMessage.IsSuccessStatusCode)
                {
                    var response = await responseMessage.Content.ReadFromJsonAsync<Response>(_jsonSerializerOptions, cancellationToken);
                    switch (response)
                    {
                        case {Success:true}:
                            return response.Rates.Select(x => new Exchange
                            {
                                Date = response.Date,
                                Rate = x.Value,
                                Currency = x.Key

                            }).ToList();

                        case {Error: { }}:
                            throw new HttpRequestException($"{_settings.BaseUri} service return error: {{Code: {response.Error.Code}, Info: {response.Error.Info}}}");
                    }
                }

                var message = $"GetExchange failed with status {responseMessage.StatusCode}. {responseMessage.ReasonPhrase}";
                throw new HttpRequestException(message);
            }
            catch (HttpRequestException)
            {
                throw;
            }
            catch (Exception exception)
            {
                throw new HttpRequestException("GetExchange failed", exception);
            }
        }

        private class Response
        {
            public bool Success { get; set; }
            
            public Error Error { get; set; }
            
            [JsonConverter(typeof(TimestampToDateTimeConverter))]
            public DateTime Timestamp { get; set; }
            
            public string Base { get; set; }
            
            [JsonConverter(typeof(StringToDateTimeConverter))]
            public DateTime Date { get; set; }
            
            public Dictionary<string, decimal> Rates { get; set; }
        }

        private class Error
        {
            public int Code { get; set; }
            public string Info { get; set; }
        }
    }
}

    
