﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ExchangeService.Api.Adapters;
using ExchangeService.Api.Dto;
using ExchangeService.Api.Infrastructure;
using ExchangeService.Api.Infrastructure.Cache;
using ExchangeService.Api.Infrastructure.Configurations;
using Microsoft.Extensions.Logging;

namespace ExchangeService.Api.Applications
{
    public class CurrencyService : ICurrencyService
    {
        private readonly ICacheProvider _cacheProvider;
        private readonly IExchangeProvider _exchangeProvider;
        private readonly ExchangeSourceSettings _settings;
        private readonly ILogger<CurrencyService> _logger;

        public CurrencyService(ICacheProvider cacheProvider, IExchangeProvider exchangeProvider, ExchangeSourceSettings settings, ILogger<CurrencyService> logger)
        {
            _cacheProvider = cacheProvider ?? throw new ArgumentNullException(nameof(cacheProvider));
            _exchangeProvider = exchangeProvider ?? throw new ArgumentNullException(nameof(exchangeProvider));
            _settings = settings ?? throw new ArgumentNullException(nameof(settings));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Exchange> GetExchangeByDays(string code, Day day, CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(code)) throw new ArgumentNullException(nameof(code));

            code = code.ToUpperInvariant().Trim();
            var date = day.GetDate();

            var cacheKey = CacheKey.GenerateKey(nameof(CurrencyService), nameof(GetExchangeByDays), date);
            _logger.LogInformation($"Generating the cache key: {cacheKey}.");

            var (isCached, cachedExchanges) = await _cacheProvider.TryGetAsync<List<Exchange>>(cacheKey, cancellationToken);

            if (isCached)
            {
                if (TryFindExchangeByCode(cachedExchanges, code, out var cachedExchange))
                {
                    _logger.LogInformation("Exchange found in cache.");
                    return cachedExchange;
                }

                await _cacheProvider.InvalidateTagAsync(nameof(CurrencyService), cancellationToken);
            }
                
            _logger.LogInformation($"Exchange not found in cache. Receive exchange from {_settings.BaseUri}");
            var apiExchanges = await _exchangeProvider.GetCurrencyAsync(date, cancellationToken);

            if (!TryFindExchangeByCode(apiExchanges, code, out var apiExchange))
                throw new InvalidOperationException($"The result does not contain the {code} currency, check the settings where the {code} currency should be in the list of received symbols {string.Join(", ", _settings.Symbols)}");

            await _cacheProvider.SetOrUpdateAsync(cacheKey, apiExchange, TimeSpan.FromDays(2), cancellationToken);
            return apiExchange;
        }

        private static bool TryFindExchangeByCode(IEnumerable<Exchange> source, string code, out Exchange exchange)
        {
            exchange = source.FirstOrDefault(x => x.Currency == code);
            return exchange != null;
        }
    }
}
