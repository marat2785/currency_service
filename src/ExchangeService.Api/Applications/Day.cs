﻿namespace ExchangeService.Api.Applications
{
    public enum Day
    {
        Today,
        Yesterday,
        DayBeforeYesterday,
        Tomorrow
    }
}
