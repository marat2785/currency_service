﻿using System.Threading;
using System.Threading.Tasks;
using ExchangeService.Api.Applications;
using ExchangeService.Api.Dto;

namespace ExchangeService.Api.Adapters
{
    public interface ICurrencyService
    {
        Task<Exchange> GetExchangeByDays(string code, Day day, CancellationToken cancellationToken);
    }
}
