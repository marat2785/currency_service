﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ExchangeService.Api.Dto;

namespace ExchangeService.Api.Adapters
{
    public interface IExchangeProvider
    {
        Task<IList<Exchange>> GetCurrencyAsync(DateTime date, CancellationToken cancellationToken);
    }
}
